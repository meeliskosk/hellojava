package driven.data;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> linkedHashMap = new LinkedHashMap<>();

        // key => value
        // Sõnaraamat
        // Maja => House
        // Isa => Dad
        // Puu => Tree


        linkedHashMap.put("Maja", "House");
        linkedHashMap.put("Ise", "Dad");
        linkedHashMap.put("Puu", "Tree");
        linkedHashMap.put("Auto", "Car");
        linkedHashMap.put("Sinine", "Blue");
        // Välja printimine
        for (Map.Entry<String, String> entry: linkedHashMap.entrySet()){
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        // Oletame, et tahan teada kuidas on inglise keeles puu

        String translation = linkedHashMap.get("Puu");

        Map<String, String> idNumberName = new LinkedHashMap<>();

        idNumberName.put("345415453","jaan");
        idNumberName.put("345435253","mari");
        idNumberName.put("345435453","jüri");

        // Kui kasutada put sama key lisamisel, kirjutatakse value üle

        // Loe lauses üle kõik erinevad tähed
        // prindi välja iga tähe järel mitu tükki seda lauses oli

        // Char on selline tüüp kus saab hoida üksikut sümbolit

        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new LinkedHashMap<>();

        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length ; i++) {
            if(letterCounts.containsKey(characters[i])){
                letterCounts.put(characters[i], letterCounts.get(characters[i])+1);
            }else{
                letterCounts.put(characters[i], 1);
            }

        }
        System.out.println();
        System.out.println(letterCounts);
        System.out.println();
        // Map.Entry <Character, Inte
        for(Map.Entry<Character, Integer> entry: letterCounts.entrySet()){
            System.out.printf("Tähte %s esines %d korda%n",entry.getKey(),entry.getValue());


        }

    }
}

