package driven.data;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[]{1, -3, 2, -23, 37, 54};

        int max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i]>max){
                max = numbers[i];
            }
        }
        System.out.println("suurim arv on " + max);

        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if(numbers[i]<min){
                min = numbers[i];
            }
        }
        System.out.println("vaiksem number on "+min);

        // Leia suurim paaritu arv
        // mitmes number see on

        max = Integer.MIN_VALUE;
        int position = 1;
        boolean oddNumbersFound = false;

        for (int i = 0; i < numbers.length ; i++) {
            if (numbers[i] % 2 != 0 && numbers[i]>max){
                max = numbers[i];
                position = i + 1;
                oddNumbersFound = true;
            }
        }
        if(oddNumbersFound){
            // suurim paaritu number on .. ja on jarjekorras ..
            System.out.printf("Suurim paaritu number on %d, mis on jrk %d%n",max, position);
        }
        else{
            System.out.println("Paaritud arvud puudvad");
        }
    }
}
