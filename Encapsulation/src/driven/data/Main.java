package driven.data;

public class Main {

    public static void main(String[] args) {
        // vaikeväärtused:
        //  int - 0
        //  boolean - false
        //  double - 0.0
        //  string - null
        //  object (Monitor) - null
        //  array - null

        Monitor firstMonitor = new Monitor();

        firstMonitor.setScreenType(ScreenType.TFT);
        firstMonitor.setManufacturer("Philips");
        firstMonitor.setDiagonal(32);
        firstMonitor.setColor(Color.BLACK);

        firstMonitor.printInfo();

        System.out.println(firstMonitor.inchToCm());
    }
}
