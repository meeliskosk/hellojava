package driven.data;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        // Loe failist iga teine rida ja kirjuta need read
        // output.txt

        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Desktop\\MK\\ValiIT\\ReadFromFile\\readFile.txt");
            // sellest loen
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // sellega kirjutan ja kuhu
            FileWriter fileWriter = new FileWriter("output.txt");

            String line = bufferedReader.readLine();
            int lineNumber = 1;

            while (line != null) {
                if(lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator());
                }
                line = bufferedReader.readLine();
                lineNumber++;
            }


            fileReader.close();
            fileWriter.close();
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
