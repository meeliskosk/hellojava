package driven.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static String pin = "2345";


    public static void main(String[] args) {

        // Lisa konto väljavõtte funktsionaalsus

        int balance = loadBalance();
        String pin = loadPin();

        pin = "2345";
        savePin(pin);


        int amount;
        loadPin();
        saveBalance(balance);


        Scanner scanner = new Scanner(System.in);
        String enteredPin;

       if (validatePin())

       do {
            boolean wrongAnswer = false;
            System.out.println("Vali toiming:");
            System.out.println("a) sularaha sisse");
            System.out.println("b) sularaha välja");
            System.out.println("c) kontojääk");
            System.out.println("d) muuda PIN kood");
            System.out.println("e) Konto väljavõte");

            String answer = scanner.nextLine();
            switch (answer){
                // kui break ära jätta, siis saab mitut varianti kasutada
                case "A":
                case "a":
                    System.out.println("Kui palju soovid sularaha sisse maksta?");
                    amount = Integer.parseInt(scanner.nextLine());
                    balance += amount;
                    saveBalance(balance);
                    saveTransaction(amount, "Withdraw");
                    System.out.printf("Uus konto jääk on: %d%n", balance+amount);
                    break;
                case "B":
                case "b":
                    System.out.println("Kui palju soovid välja võtta?\n" +
                            "a) 5 €\n" +
                            "b) 10 €\n" +
                            "c) 30 €\n" +
                            "d) muu summa");
                    answer = scanner.nextLine();
                    switch (answer) {
                        case "a":
                            amount = 5;
                            if (balance < amount) {
                                System.out.println("Pole piisavalt vahendeid!");
                            } else {
                                balance -= amount;
                                saveBalance(balance);
                                System.out.printf("Uus konto jääk on: %d%n", balance - amount);
                            }
                    }

                            break;

                case "C":
                case "c":
                    System.out.printf("Kontojääk on: %d%n", loadBalance());
                    break;
                case "d":
                case "D":
                    System.out.println("Sisesta uus PIN kood");
                    pin = scanner.nextLine();
                    savePin(pin);
                    System.out.printf("Uus PIN kood on %s%n.", pin);
                    break;
                case "e":
                case "E":
                    System.out.println("Konto välj");
                    pin = scanner.nextLine();
                    savePin(pin);
                    System.out.printf("Uus PIN kood on %s%n.", pin);
                    break;
                default:
                    System.out.println("Selline valik puudub");
                    wrongAnswer = true;
                    break;
                } while (wrongAnswer);
                System.out.println(("Kas soovid veel tehinguid teha? jah, ei?"));

        } while (scanner.nextLine().equals("jah"));

    }


    static void savePin(String pin){
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("PIN koodi salvestamine ebaõnnestus");
        }
    }
    static String loadPin(){
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("PIN laadimine ebaõnnestus");;
        }
        return pin;
    }
    static void saveBalance(int balance){
        try {
            FileWriter fileWriter = new FileWriter("balance.txt", true);
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }

    static int loadBalance(){
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = (Integer.parseInt(bufferedReader.readLine()));
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");;
        }
        return balance;
    }

    static boolean validatePin(){
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Sisesta PIN kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(pin)){
                System.out.println("PIN kood õige!\n\n");
                return true;
            }
        }
        System.out.println("Kaart blokeeritud!");
        return false;
    }

//    static String currentDateTimeToString(){
//        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy EEEEE");
//        Date date = new Date();
//
//        Calendar cal = Calendar.getInstance();
//        System.out.println(dateFormat.format(date));
//
//        return date.;
//    }

    static void saveTransaction(int amount, String transaction) {


        if(transaction.equals("Deposit")){

        }
        else {

        }

    }

}