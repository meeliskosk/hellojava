package driven.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy EEEEE");

        Calendar calendar = Calendar.getInstance();

        // Move calendar to yesterday
        // calendar.add(Calendar.DATE, -1);

        // Get current date of calendar which point to yesterday now
        Date date = calendar.getTime();

        System.out.println(dateFormat.format(date));

        // calendar.add(Calendar.YEAR, 1);

        Date year = calendar.getTime();

        System.out.println(dateFormat.format(year));

        // Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad
        System.out.println(calendar.get(Calendar.MONTH));
        // Selle aasta selle kuu esimene kuupäev
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        Date date2 = calendar.getTime();

        int monthsLeftInThisYear = 11 - calendar.get(Calendar.MONTH) ;
        DateFormat weekDay = new SimpleDateFormat( "EEEEE");
        for (int i = 0; i < monthsLeftInThisYear ; i++) {
            calendar.add(Calendar.MONTH, 1);
            Date date3 = calendar.getTime();
            System.out.println(weekDay.format(date3));

        }



    }

}
