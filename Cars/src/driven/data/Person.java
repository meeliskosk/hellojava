package driven.data;

enum Gender {FEMALE, MALE, NOTSPECIFIED}




public class Person {


    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;



    @Override
    public String toString() {
        return String.valueOf(gender);
    }

    public Person(String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;


    }


    public String getFirstName(){
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }


    // Kui klassile ise lisada mingi konstruktor, siis see nähtamatu parameetrita konstruktor kustutatakse
    // kui seda tahan, siis pean ise tegema -- public Person(){}
    // Sellest klassist saab teha objekti ainult selle uue konstruktoriga.



}
