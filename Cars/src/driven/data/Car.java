package driven.data;

import java.util.ArrayList;
import java.util.List;

enum Fuel {GAS, DIESEL, PETROL, HYBRID, ELECTRIC}


public class Car {

    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;
    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;
    private boolean isCarParked;
    private Person owner;
    private Person driver;
    private int maxPassengers;
    private int totalPassengers;
    private List<Person> passengers = new ArrayList<Person>();

    public int getTotalPassengers() {
        return totalPassengers;
    }
    public void setTotalPassengers(int totalPassengers) {
        this.totalPassengers = totalPassengers;
    }
    public List<Person> getPassengers() {
        return passengers;
    }
    public void setPassengers(List<Person> passengers) {
        this.passengers = passengers;
    }
    public int getMaxPassengers() {
        return maxPassengers;
    }
    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }
    public Person getOwner() {
        return owner;
    }
    public void setOwner(Person owner) {
        this.owner = owner;
    }
    public Person getDriver() {
        return driver;
    }
    public void setDriver(Person driver) {
        this.driver = driver;
    }
    // Konstruktor
    // eriline meetod mis käivitatakse klassist objekti loomisel
//    public Car(){
//
//    }
//    // Consutructor overloading
//    public Car(String make, String model, int year, int maxSpeed){
//        this.make = make;
//        this.model = model;
//        this.year = year;
//        this.maxSpeed = maxSpeed;
//    }
//
//    public Car(String make, String model, int year, int maxSpeed, boolean isUsed,Fuel fuel){
//        this.make = make;
//        this.model = model;
//        this.year = year;
//        this.maxSpeed = maxSpeed;
//        this.isUsed = isUsed;
//        this.fuel = fuel;
//    }
    public Car(String make, int maxSpeed, int maxPassengers, int totalPassengers){
        this.make = make;
        this.maxSpeed = maxSpeed;
        this.maxPassengers = maxPassengers;
        this.totalPassengers = totalPassengers;
    }
    public void passengerList(){
////        for (int i = 0; i < passengers.size() ; i++) {
////            System.out.println(passengers.get(i).getFirstName());
//        }
        // Foreach loop
        // Iga listis oleva elemendi kohta, tekitab objekt passenger
        // 1. kordus Person passenger on esimene reisija
        // 2. kordus Person passenger on teine reisija
        for (Person passenger: passengers){
            System.out.println(passenger.getFirstName());
        }
    }
//    public Car(String make, int maxSpeed){
//        this.make = make;
//        this.maxSpeed = maxSpeed;
//        this.person = person;
//    }
    public void startEngine(){
        if(!isEngineRunning){
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }
    public void stopEngine(){
        if(isEngineRunning){
            isEngineRunning = false;
            System.out.println("Mootor on seisatud");
        } else {
            System.out.println("Mootor juba seisatud");
        }
    }
    public void accelerate(int targetSpeed){
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta!");
        } else if (targetSpeed > maxSpeed){
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto max kiirus on %d%n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto uus kiirus on %d%n", targetSpeed);
        }
    }
    public void slowDown(int targetSpeed){
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta!");
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustada suuremale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto uus kiirus on %d%n", targetSpeed);
        }
    }
    public void addPassenger(Person passenger){
        if(passengers.size() < maxPassengers){
            passengers.add(passenger);
            System.out.printf("Autosse lisati reisija %s%n", passenger.getFirstName());
        } else {
            System.out.println("Autosse ei mahu enam reisijaid");
        }

    }
    public void removePassenger (Person passenger){
        if(passengers.indexOf(passenger) != -1){
            passengers.remove(passenger);
        } else {
            System.out.println("Autos sellist reisijat ei ole");
        }
    }
    public void park() {
        slowDown(0);
        stopEngine();
    }
}

    // slowDown (int targetSpeed)
    // park() , mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid -- slowDown ja stopEngine)
    // lisa autole parameeter driver ja owner (Person tüübist) ja nendele get/set meetodid
    // loo mõni auto objekt, kellel on määratud kasutajad ja omanik
    // prindi välja auto omaniku vanus

    // lisa autole max reisijate arv
    // lisa autole võimalus hoida reisijad
    // lisa meetod reisijate lisamiseks ja eemaldamiseks autost
    // kontrolli, et reisijaid ei lisataks rohkem kui mahub


