package driven.data;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

//        Car bmw = new Car();
//        bmw.startEngine();
//        bmw.accelerate(100);
//        bmw.stopEngine();
//
//        Car fiat = new Car();
//        Car mercedes = new Car();
//        Car opel = new Car("Opel","Vectra",1999,140);
//        opel.startEngine();
//        opel.accelerate(200);

//        Person person = new Person("meelis","kosk",33, UserType.OWNER);
//
//        Car skoda = new Car("octavia",217, person);
//        skoda.startEngine();
//        skoda.accelerate(60);
//        skoda.slowDown(0);
//        skoda.stopEngine();
//        skoda.park();
//        System.out.println("Auto omaniku vanus on " + person.getAge());

        Person passenger1 = new Person("Mari","",Gender.FEMALE,20);
        Person passenger2 = new Person("Jüri","",Gender.MALE, 32);
        Person passenger3 = new Person("Peeter","",Gender.MALE,25);
        Person passenger4 = new Person("anna","",Gender.FEMALE, 43);
        Person passenger5 = new Person("Karl","",Gender.MALE,34);


        Car bmw = new Car("X5", 300,5,0);
        bmw.addPassenger(passenger1);
        bmw.addPassenger(passenger2);
        bmw.addPassenger(passenger3);
        bmw.addPassenger(passenger4);
        bmw.addPassenger(passenger5);
        bmw.passengerList();
        bmw.removePassenger(passenger3);
        bmw.setMaxPassengers(2);

        Person juku = new Person("juku","pupu",Gender.MALE,32);
        System.out.println(juku);

        }


}

