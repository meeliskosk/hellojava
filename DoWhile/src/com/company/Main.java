package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Do while tsükkel on nagu while tsükkel ainult et
        // kontroll tehakse peale esimest kordust
     /*   System.out.println("Kas tahad jätkata? Jah/ei");
       // Jätkame seni, kuni kasutaja kirjutab "ei"

       Scanner scanner = new Scanner(System.in);

       String answer = scanner.nextLine();

       while(!answer.equals("ei")) {
           System.out.println("Kas tahad jätkata? Jah/ei");
           answer = scanner.nextLine();
     }*/

     Scanner scanner = new Scanner(System.in);
     String answer;
     // Iga muutuja, mille me deklareerime kehtib ainult,
     // Seda ymbritsevate {} sees

     do {
         System.out.println("Kas tahad jatkata? Jah/ei");
         answer = scanner.nextLine();
     } while
     (!answer.equals("ei"));
    }
}
