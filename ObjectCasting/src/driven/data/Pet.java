package driven.data;

public class Pet extends DomesticAnimal {

    public boolean isFromShelter() {
        return fromShelter;
    }

    public void setFromShelter(boolean fromShelter) {
        this.fromShelter = fromShelter;
    }

    private boolean fromShelter;

    public void attackOwner(){

    }

    @Override
    public void printInfo(){
        super.printInfo();
        System.out.printf("Koduloom on varjupaigast: %s%n", fromShelter);
    }
}
