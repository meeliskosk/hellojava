package driven.data;

public class Cat extends Pet {
    private boolean hasFur = true;
    public boolean isHasFur() {
        return hasFur;
    }
    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }
    public void catchMouse(){
        System.out.println("Püüdsin hiire kinni");
    }

    @Override
    public void eat(){
        System.out.println("limpsin piima");
    }
    @Override
    public void printInfo(){
        super.printInfo();
        System.out.printf("Karvade olemasolu: %s%n",hasFur);
//        System.out.println("Looma info:");
//        System.out.printf("Nimi: %s%n", name);
//        System.out.printf("Tõug: %s%n", breed);
//        System.out.printf("Vanus: %d%n", age);
//        System.out.printf("Kaal: %.2f%n", weight);
    }
}
