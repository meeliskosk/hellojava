package driven.data;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Iga kassi saab võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // Iga loom ei ole kass
        // explicit casting
//        Cat cat = (Cat) new Animal();

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("pontu");
        WildAnimal karu = new WildAnimal();
        karu.setName("Mõmmi");

        animals.add(dog);
        dog.setName("Muki");
        animals.add(karu);
        animals.add(new Pig());
        animals.get(animals.size()- 1).setName("Piggy");
        animals.add(new Cow());
        animals.get(animals.size()- 1).setName("Vissi");

        karu.setBreed("Pruunkaru");
        karu.setHabitat("Eesti");
        dog.setOwnerName("Jaan");

        // Kutsu kõikide listis loomade printinfo

//        for (int i = 0; i < animals.size() ; i++) {
//            animal.printInfo();
//            System.out.println();
//        }
        System.out.println();
        for (Animal animalInList: animals){
            animalInList.printInfo();
            System.out.println();
        }

        Animal secondAnimal = new Dog();
        ((Dog)secondAnimal).setHasTail(true);

    }
}
