package driven.data;

public class DomesticAnimal extends Animal {

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    private String ownerName;
    public void runAwayFromHome(){

    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Omaniku nimi on: %s%n", ownerName);
    }
}
