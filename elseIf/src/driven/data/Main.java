package driven.data;

import java.util.Scanner;

public class Main {

// Else käib ainult selle if kohta millega ta on ühes blokis

    public static void main(String[] args) {
        System.out.printf("Sistesta arv%n");

        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());


        if (number > 3) {
            System.out.printf("%d on suurem kui kolm%n", number);
        }
        else if (number == 0) {
            System.out.printf("%d = 0'ga%n", number);
        }
        else if (number < 3) {
            System.out.printf("%d on väiksem kui kolm%n", number);
        }
        else {
            System.out.printf("%d = kolm%n", number);
        }
    }
}
