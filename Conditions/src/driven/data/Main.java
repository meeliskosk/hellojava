package driven.data;

import java.io.PrintStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.printf("Sisesta number%n");
        // kutsun esile scanner meetodi, et saaks arvu sisestada
        Scanner scanner = new Scanner(System.in);
        // teeme parseInt meetodiga scannerisse sisestatud string'ist arvu ja läheme järgmisele reale
        // üks = alati deklareerib muutuja, kaks == kontrollib võrdsust
        int a = Integer.parseInt(scanner.nextLine());

        // nüüd sisestame konditsiooni
        if (a == 3) {
            System.out.printf("%d on võrdne kolmega%n", a);
        } else {
            System.out.printf("%d ole võrdne kolmega%n", a);
        }
        if (a < 5) {
            System.out.printf("%d on väiksem viiest%n", a);
        } else {
            System.out.printf("%d on suurem viiest%n", a);
        }
        if (a != 8) {
            System.out.printf("%d ei võrdu kaheksaga%n", a);
        } else {
            System.out.printf("%d võrdub kaheksaga%n", a);
        }
        if (a >= 10) {
            System.out.printf("%d on suurem või võrdne kümnega%n", a);
        }
        // && on 'ja' tingimus, nt kui vaja kontrollida kas arv on kahe arvu vahel
        if (a > 2 && a < 8) {
            System.out.printf("%d on kahe ja kaheksa vahel%n", a);
        }

        // || on "või" tingimus
        if (a < 5 || a > 10) {
            System.out.printf("%d on väiksem kui viis või suurem kui kümme",a);
        }
        // tehete järjekord oluline!
        // 'ja' tehakse enne kui 'või', võib suluda lisada parema lugemise jaoks
        // kui arv on 2 ja 8 vahel või viie ja kaheksa vahel või suurem kui 10
        // 'ei ole' tähistame '!' märgiga

        if ((a > 2 && a < 8) || a > 10) {
            System.out.printf("%d on kahe ja kaheksa vahel või suurem kui 10%n", a);
        }

        // Kui arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel
        // või arv on negatiivne aga pole suurem kui 14

        if((!(a > 4 && a < 6) || (a < 0 && !(a > -14))) {

        }
        // kui esimene tingimus on vale, siis järgmisi tingimusi ei vaadata.
        // seetõttu tasub kõige rohkem välistav tingimus esimeseks panna.
    }
}
