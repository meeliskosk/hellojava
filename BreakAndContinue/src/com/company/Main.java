package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    /*
	    Kysime kasutajalt pin koodi
	    kui see on oige, siis ytle "Tore"
	    muul juhul kysi uuesti
	    kokku 3 korda
	     */

	    String realPin = "1234";

        Scanner scanner = new Scanner(System.in);
        // break peatab tsykli
        for (int i = 0; i < 3 ; i++) {
            System.out.println("Sisesta PIN kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(realPin)) {
                System.out.println("Oige PIN!");
                break;
            }
        }

        // prindi valja 10 kuni 20 ja 40 kuni 60
        // Continue jatab selle tsyklikorduse katki ja laheb jargmise korduse juurde
        for (int i = 10; i <=60 ; i++) {
            if(i > 20 && i < 40) {
                continue;
            }
            System.out.println(i);
        }
    }
}
