package driven.data;

import java.util.List;

public class Country {

    private String population;
    private String name;
    private List<String> langSpoken;

    @Override
    public String toString() {
        return String.join(" ", "Riik: " + name +", rahvaarv: " + population +" ja enimkõneldud keeled: "+ langSpoken );
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLangSpoken() {
        return langSpoken;
    }

    public void setLangSpoken(List<String> langSpoken) {
        this.langSpoken = langSpoken;
    }




}
