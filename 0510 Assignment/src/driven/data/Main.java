package driven.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    @Override
    public String toString() {
        return super.toString();
    }

    public static void main(String[] args) {

        System.out.println("Ülesanne 1: ");

        double pi = Math.PI;
        System.out.println(pi*2 + System.lineSeparator());

        System.out.println("Ülesanne 2:");

        int a = 2;
        int b = 1;

        boolean areEqual = intsAreEqual(a,b);
        System.out.println(areEqual + System.lineSeparator());


        System.out.println("Ülesanne 3:");

        String[] string = {"Ei", "oska", "neid", "stringe!"};
        String sentence = Arrays.toString(string);
        System.out.println(sentence + System.lineSeparator());

        System.out.println("Ülesanne 4:");

        int year = 2018;

        byte century = calculateCentury(year);
        System.out.println(century + System.lineSeparator());

        System.out.println("Ülesanne 5: ");

        Country country = new Country();
        List<String> langSpoken = new ArrayList<>();

        country.setName("USA");
        country.setPopulation("350 000 000");
        langSpoken.add("Enlish");
        langSpoken.add("Spanish");
        langSpoken.add("Polish");

        country.setLangSpoken(langSpoken);

        System.out.println(country.toString());


    }

    static boolean intsAreEqual(int a, int b){
        return(a == b);
    }

//    static int[] countString (String sentence){
//
//
//        char[] count = sentence.toCharArray();
//        for (int i = 0; i < count.length; i++) {
//            while (i < count.length && count[i] != " "){
//
//            }
//
//        }
//
//    }

    static byte calculateCentury(int year){
        int century = year / 100 +1;
        if (year > 2018 || year < 1){
            return -1;
        } else{
            return (byte)century;
        }
    }


}
