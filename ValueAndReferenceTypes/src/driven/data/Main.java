package driven.data;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Siin jääb b väärtus 3'ks, sest kui primitiiv tüüpi muutuja panne võrduma teise primitiiviga
        // siis tehaks arvuti mällu uus muutuja koos antud väärtusega

        // sama juhtub primitiiv muutuja kaasa andmisel
        // meetodi parameetriks (tehaks arvuti mällu uus muutuja koos antud väärtusega)
//
//        int a = 3;
//	    int b = a;
//	    a = 7;
//        System.out.printf("b väärtus on %s%n", b);
//        increment(b);
//        System.out.printf("b on endiselt %s%n", b);
//
//        // Kui panna üks reference-type (viitamise tüüpi) muutuja võrduma teise
//        // muutujaga, siis tegelikult jääb mälus ikkagi alles ainult üks muutuja
//        // teine muutuja hakkab viitama samale kohale mälus
//        // meil on kaks muutujat, aga tegelikult on täpselt sama objekt
        int[]numbers = new int[]{-5, 4};
        int[] secondNumbers = numbers;
//        numbers[0] = 3;
//
//        System.out.println(secondNumbers[0]);

        Point pointA = new Point();
        pointA.x = 50;
        pointA.y = 3;

        Point pointB = new Point();
        pointB = pointA;
//        pointB.x = 7;
//
//        System.out.println(pointA.x);

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

//        pointA.y = -4;
//
//        System.out.println(points.get(0).y);
//        System.out.println(points.get(1).y);
//        System.out.println(pointB.y);
//
        increment(pointA);
        System.out.println();
        Point pointC = new Point();
        pointC.x = 12;
        pointC.y = 20;
        increment(pointC);
        System.out.println();
        increment(numbers);


        pointC.increment();


//
//        pointA.printNotStatic();
//
//        Point.printStatic();
    }

//    public static void increment(int a){
//        a++;
//        System.out.printf("a on nüüd %d%n",a);
//    }
    // Suurenda x ja y koordinaate ühe võrra
    public static void increment(Point points){
        points.x++;
        points.y++;
        System.out.println(points.x);
        System.out.println(points.y);
    }
    // suurenda kõiki elemente 1 võrra
    public static void increment(int[] numbers){
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;
        }
        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
    }
}
