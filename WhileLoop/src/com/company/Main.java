package com.company;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // lõpmatu tsükkel

        //while (true) {
        //    System.out.println("tere");
        //}

        Scanner scanner = new Scanner (System.in);

        do {
            Random random = new Random();
            // random.nextInt(5) genereerib numbri 0 kuni 4

            int number = random.nextInt(5) + 1;
            // teine võimalus sama asja teha
            // int number = ThreadLocalRandom.current().nextInt(1, 6);

            System.out.println("Arva ära number 1 kuni 5");

            int enteredNumber = Integer.parseInt(scanner.nextLine());

            while (enteredNumber != number) {
                System.out.println("Proovi uuesti!");
                enteredNumber = Integer.parseInt(scanner.nextLine());
            }

            System.out.println("Tubli! Arvasid ära!");
            System.out.println("Kas soovid veel mängida?");

        } while (!scanner.nextLine().toLowerCase().equals("ei"));
    }
}
