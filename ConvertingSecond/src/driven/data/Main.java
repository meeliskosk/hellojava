package driven.data;

public class Main {

    public static void main(String[] args) {

        // konverteerime stringi numriks ja vastupidi
	    double a = Double.parseDouble("12.12");
	    // defineerime numbri stringina
	    String number = "12.23";
	    // double muutuja b on defineeritud kui string number väärtus double väärtusena
	    double b = Double.parseDouble(number);

        System.out.println(a);
        System.out.println(b);

        // number'it võib mitu korda kasutada.. ei ole probleemi
        number = String.valueOf(a);
        System.out.println(number);

        // konverdime long'ist stringi
        long c = 340000000000L;
        number = String.valueOf(c);
        System.out.println(number);

        // teeme stringist byte'i
        number = "-2";
        byte d = Byte.parseByte(number);
        System.out.println(d);

        //teeme string'ist short'i
        short e = Short.parseShort(number);
        System.out.println(e);

        // teeme string'ist float'i
        float f = Float.parseFloat(number);
        System.out.println(f);


    }
}
