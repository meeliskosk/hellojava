package driven.data;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /* Try-Catch tasub panna võimalikult väikse koodiploki ümber
         */
        int a = 2;
        try {
            String word = "tere";
            int b = 4 / a;
            word.length();

        /*
         catch ploke võib mitu olla
         nad peaksid olema vastavalt pärimusele detailsemast vähem detailseks
         nt 0'ga jagamisel tekib artimeetiline viga mis pärineb Runtime'st,
         mis oma korda pärineb Exception'ist
         kui on mitu Catch plokki, siis otsib ta esimese ploki,
         mis oskab antud Exceptioni kinni püüa.
         e tähistab exception muutujat ning seda on võimalik välja kutsuda, et näha täpsemat sisu
        */
        } catch (ArithmeticException e) {
            if (e.getMessage().equals("/ by zero")) {
                System.out.println("Nulliga ei saa jagada");
            } else {
                System.out.println("Aritmeetiline viga");
                System.out.println(e.getMessage());
            }
        } catch(RuntimeException e){
                System.out.println("Reaalaja viga");

        } catch(Exception e){
                /* Exception on klass, millest kõik erinevad Exception tüübid
                   pärinevad. Mis omakorda tähendab, et püüdes kinni
                   selle üldise exceptioni, püüame kinni kõik exceptionid
                */
                System.out.println("Esines viga!");
            }
        } {
        /* Küsi kasutajalt numbrit ja näita veateadet
        */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta number!");
        boolean correctNumber = false;
        do {
            try {
                int userInput = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Sisesta päris number");
                }
        } while (!correctNumber);
        /* Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        ja näita veateadet!
        */
        try {
            int[] numbers = new int[]{1, 2, 3, 4, 5};
            numbers[7] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeksit, kuhu tahtsid väärtust määrata," +
                    "ei eksisteeri");
        }
        catch (Exception e){
            System.out.println("Esines tundmatu viga");
        }
    }
}
