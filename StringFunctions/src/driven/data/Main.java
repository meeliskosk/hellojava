package driven.data;

public class Main {

    public static void main(String[] args) {

        String sentence = "kevadel elutses metsas Mutionu keset kuuski noori vanu";

        // Sümbolite indeksid tekstis algavad samamoodi indekiga 0
        // nagu massiivides

        // Leia üles esimene tühik, mis on tema indeks
        int spaceIndex = sentence.indexOf(" ");
        // indexOf tagastab -1, kui otsitavat fraasi (sümbolit või sõna) ei leitud
        // ning indeksi (kust sõna algab) kui fraas leitakse


        // Prindi välja kõigi tühikute indeksid lauses
        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);

        }

        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);

        }

        spaceIndex = sentence.indexOf(" ");
        // Prindi lause esimesed 4 tähte
        String part = sentence.substring(0, spaceIndex);
        System.out.println(part);

        // Prindi lause teine sõna
        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);

        String secondWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(secondWord);


        // Leia esimene k-tähega algav sõna
        // et leiaks ka siis, kui see sõna on lause esimene sõna

        String firstLetter = sentence.substring(0, 1);

        String kWord = "";

        if(firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);
        }
        else {
            int kIndex = sentence.indexOf(" k") + 1;
            if(kIndex != 0) {
                spaceIndex = sentence.indexOf(" ", kIndex);

                kWord = sentence.substring(kIndex, spaceIndex);
            }
        }
        if(kWord.equals("")) {
            System.out.println("Puudub k-tähega algav sõna");
        }
        else {
            System.out.printf("Esimene k-tähega algav sõna on %s%n", kWord);
        }





        // Leia mitu sõna mul lauses on
        int spaceCounter = 0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter + 1);



        // Leia mitu k-tähega algavat sõna on lauses


        // Siin loendame mida vaja
        int kCounter = 0;

        int kIndex = sentence.indexOf(" k");
        while (kIndex != -1) {
            kIndex = sentence.indexOf(" k", kIndex + 1);
            kCounter++;
        }

        if(sentence.substring(0, 1).equals("k")) {
            kCounter++;
        }

        System.out.printf("K algusega sõnade arv lauses on %d%n", kCounter);
    }
}
