package com.company;

public class Main {

    public static void main(String[] args) {
	    String sentence = "Kevadel elutses metsas mutionu keset Kuuski noori vanu";
	    // symbolite indeksid tekstis algavad samamoodi indeksiga 0
        // nagu massiivides

        // Leia yles esimene tyhik ning mis on tema index

        int spaceIndex = sentence.indexOf(" ");
        System.out.println(spaceIndex);
        System.out.println();
        // indexOf tagastab -1 kui otsitavat fraasi voi symbolit ei leita

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }
        System.out.println();
        // indexid tagurpidi
        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1){
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);
        }
        System.out.println();

        spaceIndex = sentence.indexOf(" ");
        // prindi lause esimesed 4 tahte
        String part = sentence.substring(0,spaceIndex);
        System.out.println(part);

        System.out.println();
        // prindi lause teine sona
        int secondSpaceIndex = sentence.indexOf(" ",spaceIndex + 1);

        String secondWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(secondWord);
        System.out.println();

        // Leia esimene k-tahega algav sona
        // et leiaks ka siis kui on lause esimene sona

        String firstLetter = sentence.substring(0,1);
        String kWord = "";

        if(firstLetter.equals("k")){
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0,spaceIndex);
        }
        else {
            int kIndex = sentence.indexOf(" k") + 1;
            if(kIndex != 0){
                spaceIndex = sentence.indexOf(" ",kIndex);
                kWord = sentence.substring(kIndex,spaceIndex);
            }
        }
        if(kWord.equals(" ")){
            System.out.println("Puudub k tahega sona");
        }
        else {
            System.out.printf("Esimene k tahega sona on %s%n",kWord);
        }
    }















}
