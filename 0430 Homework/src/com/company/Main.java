package com.company;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Koosta täisarvude massiiv kümnest arvust
        // Kirjuta faili kõik suuremad arvud kui kaks


        int array[] = {1,2,3,4,5,6,7,8,9,10};
        try {
            FileWriter fileWriter = new FileWriter("array.txt");
            for (int i = 0; i < array.length ; i++) {
                if(array[i] > 2){
                    fileWriter.write(array[i] + System.lineSeparator());
                }
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Küsi kasutajalt kaks arvu seni kuni mõlemad on korrektsed (ei ole tekst, numbriks teisendatavad)
        // ning nende summa ei ole paarisarv


        int x = 0;
        int y = 0;

        boolean correctNumber = false;

        do {
            do {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Sisesta esimene taisarv(numbrina)");
                try {
                    x = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Sisesta korrektne number!");
                }
            } while (!correctNumber);

            correctNumber = false;

            do {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Sisesta teine taisarv(numbrina)");
                try {
                    y = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Sisesta korrektne number!");
                }
            } while (true);
        } while ((x + y) % 2 == 0);


        // 3. Küsi kasutajalt mitu arvu ta tahab sisestada
        // seejärel küsi ühe kaupa kasutajalt need arvud
        // ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde (Append)
        // Tulemus on iga programmi käivitamisel järel on failis kõik eelnevad summad kirjas.

        Scanner s = new Scanner(System.in);
        System.out.print("Sisesta mitu arvu soovid sisestada:");
        int n = s.nextInt();
        int massiiv[] = new int[n];
        System.out.println("Sisesta arvud:");
        int sum = 0;
        for (int i = 0; i < n; i++) {
            massiiv[i] = s.nextInt();
            sum = sum + massiiv[i];
        }
        System.out.println("Sum:" + sum);

        try {
            FileWriter fileWriter = new FileWriter("summa.txt", true);
            fileWriter.append(sum + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
}
