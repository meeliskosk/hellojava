package driven.data;

public class Main {

    public static void main(String[] args) {

        // Deklareerime tekst tüüpi muutuja (String) muutuja (variable)
        // mille nimeks paneme 'name' ja väärtuseks 'Meelis'
        String name = "Meelis";
        String lastName = "Kosk";
        System.out.println("Hello, " + name + " " + lastName + "!");
    }
}
