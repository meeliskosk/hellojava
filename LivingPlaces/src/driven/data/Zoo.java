package driven.data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {

    // Siin hoitakse infot palju meil igat looma loomaias on
    private Map<String, Integer> animalCounts = new LinkedHashMap<>();

    // Siin hoitakse infot kui palju meil igat looma loomaaeda mahub
    private Map<String, Integer> maxAnimalCounts = new LinkedHashMap<>();
    private List<Animal> animals = new ArrayList<Animal>();

    public Map<String, Integer> getAnimalCounts() {
        return animalCounts;
    }

    public void setAnimalCounts(Map<String, Integer> animalCounts) {
        this.animalCounts = animalCounts;
    }

    public Zoo(){
        maxAnimalCounts.put("Lion", 6);
        maxAnimalCounts.put("Giraffe", 3);

    }

    @Override
    public void removeAnimal(String animalType) {

    }

    @Override
    public void printAnimalCounts() {

    }

    @Override
    public void addAnimal(Animal animal) {

        //Kas animal on tüübist Pet või pärineb sellest tüübist

        if(Pet.class.isInstance(animal)){
            System.out.println("Lemmiklooma loomaaeda panna ei saa");
            return;
        }
        String animalType = animal.getClass().getSimpleName();
        System.out.println(animalType);

        if(!maxAnimalCounts.containsKey(animalType)){
            System.out.println("Farmis selline farmiloom elada ei saa!");
            return;
        }

        if(animalCounts.containsKey(animalType)){
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount){
                System.out.println("Farmi ei mahu sellist looma enam");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) +1);
            // Sellist looma veel ei ole farmis
            // ja sellele loomale kohta on
        } else{
            animalCounts.put(animalType, 1);
        }
        animals.add((Animal)animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);

    }
}
