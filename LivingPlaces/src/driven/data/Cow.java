package driven.data;

public class Cow extends FarmAnimal {

    public int getAverageDailyMilk() {
        return averageDailyMilk;
    }

    public void setAverageDailyMilk(int averageDailyMilk) {
        this.averageDailyMilk = averageDailyMilk;
    }

    private int averageDailyMilk;

    public void milking(){
        System.out.println("My job is to give milk");
    }

}
