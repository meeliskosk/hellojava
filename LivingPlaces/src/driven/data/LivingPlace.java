package driven.data;

public interface LivingPlace {

    void removeAnimal (String animalType);
    void printAnimalCounts();
    void addAnimal(Animal animal);

}
