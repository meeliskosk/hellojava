package driven.data;

public class Main {

	// Täienda ja zoo ja forest klasse nii, et neil oleks nende 3 meetodi sisu

    public static void main(String[] args) {
	    LivingPlace livingPlace = new Zoo();
	    Pig pig = new Pig();
	    pig.setName("Kalle");


	    livingPlace.addAnimal(pig);
		livingPlace.addAnimal(new Pig());
		livingPlace.addAnimal(new Cow());
		Cow cow = new Cow();
		cow.setName("Priit");
		livingPlace.addAnimal(cow);

	    livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Pig");
		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();
		livingPlace.addAnimal((pig));
		livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Pig");
		livingPlace.removeAnimal("Pig");

    }
}
