package driven.data;

public class WildAnimal extends Animal {
    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    private String habitat;
    public void runWild(){
        System.out.println("I'm running wild!");
    }

    @Override

    public String getName(){
        return "Metsloomal pole nime";
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("WildAnimal elab: %s%n", habitat);
    }
}
