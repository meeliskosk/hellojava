package driven.data;

import java.util.*;

public class Farm implements LivingPlace {

    // Siin hoitakse infot palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new LinkedHashMap<>();
    // Siin hoitakse infot kui palju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new LinkedHashMap<>();
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();

    public Map<String, Integer> getAnimalCounts() {
        return animalCounts;
    }

    public void setAnimalCounts(Map<String, Integer> animalCounts) {
        this.animalCounts = animalCounts;
    }

    public Farm(){
        maxAnimalCounts.put("Pig", 2);
        maxAnimalCounts.put("Cow", 2);
        maxAnimalCounts.put("Sheep", 15);
    }
    @Override
    public void addAnimal(Animal animal){
        //Kas animal on tüübist FarmAnimal või pärineb sellest tüübist

        if(!FarmAnimal.class.isInstance(animal)){
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return;
        }
        String animalType = animal.getClass().getSimpleName();
        System.out.println(animalType);

        if(!maxAnimalCounts.containsKey(animalType)){
            System.out.println("Farmis selline farmiloom elada ei saa!");
            return;
        }

        if(animalCounts.containsKey(animalType)){
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount){
                System.out.println("Farmi ei mahu sellist looma enam");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) +1);
        // Sellist looma veel ei ole farmis
        // ja sellele loomale kohta on
        } else{
            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal)animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);

    }
    @Override
    // meetod mis prindib välja kõik farmi loomad ja kui palju neid on
    public void printAnimalCounts(){
        for(Map.Entry <String, Integer> entry: animalCounts.entrySet()){
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
    @Override
    // meetod mis kustutab farmist looma, nt Pig
    public void removeAnimal (String animalType){
        // Kuidas leida loom kelle nimi on 'Kalle'

        // Loon uue muutuja 'farmAnimal' mis käib listi läbi
        // FarmAnimal on list ning animal muutuja mis listi läbi käib
        // : animals --- parameeter mida soovin läbi käia
        boolean animalFound = false;
        for(FarmAnimal farmAnimal: animals) {
            // kui ei leidu matchi


            // kui leidub match, kustutab ta selle ära
            if (farmAnimal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(farmAnimal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);
            }
            if(animalCounts.get(animalType) == 1) {
                animalCounts.remove(animalType);
                System.out.println("Viimane loom eemaldatud");
            }
            else {
                animalCounts.put(animalType, animalCounts.get(animalType) - 1);
            }
            animalFound = true;
            break;
        }
        if(!animalFound){
            System.out.println("Farmis antud loom puudub");
        }
    }

}
