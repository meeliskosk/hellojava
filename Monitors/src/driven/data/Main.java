package driven.data;

public class Main {

    public static void main(String[] args) {

        Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.BLACK;
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.OLED;

        secondMonitor.manufacturer = "Samsung";
        secondMonitor.color = Color.WHITE;
        secondMonitor.diagonal = 22;
        secondMonitor.screenType = ScreenType.TFT;

        thirdMonitor.manufacturer = "LG";
        thirdMonitor.color = Color.RED;
        thirdMonitor.diagonal = 24;
        thirdMonitor.screenType = ScreenType.PLASMA;

        firstMonitor.color = Color.BLACK;

        Monitor[] monitors = new Monitor[4];

        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;
        monitors[3] = new Monitor();
        monitors[3].manufacturer = "Sony";
        monitors[3].color = Color.GRAY;
        monitors[3].diagonal = 28;
        monitors[3].screenType = ScreenType.LCD;

        Monitor maxSizeMonitor = monitors[0];

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > maxSizeMonitor.diagonal) ;
            maxSizeMonitor = monitors[i];
        }
        maxSizeMonitor.printInfo();
        firstMonitor.printInfo();

        System.out.printf("Monitori diagonaal cm-tes on %.2f%n", maxSizeMonitor.inchToCm());
    }
}

