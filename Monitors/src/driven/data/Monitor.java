package driven.data;

import java.sql.SQLOutput;

// Tüüp, milles saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na.
enum Color {
    BLACK,
    WHITE,
    RED,
    GRAY
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    PLASMA,
}

public class Monitor {
        String manufacturer;
        double diagonal; // tollides
        Color color;
        ScreenType screenType;

      void printInfo(){
            System.out.println();
            System.out.println("Monitori info;");
            System.out.printf("Tootja: %s%n", manufacturer);
            System.out.printf("Diagonaal: %.1f%n", diagonal);
            System.out.printf("Värv: %s%n", color);
            System.out.printf("Ekraani tüüp: %s%n", screenType);
            System.out.println();
      }

      double inchToCm() {
          return diagonal * 2.54;
      }




}
