

package driven.data;

// import tähendab, et antud klassile 'Main' lisatakse ligipääs java class library
// java.util 'is paiknevale klassile 'scanner'
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // klassist objekti tegemine - loome Scanner tüübist objekti nimega scanner, mille kaudu saab arvuti kasutaja
        // sisendit lugeda
        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis on Sinu nimi?");
        // scanner objekti saab kasutada muutujate loomiseks, nt siin 'name' muutuja, mida kasutaja sisestab
        String name = scanner.nextLine();
        // siin trükib arvuti sisestatud muutuja välja
        System.out.println("Tere, " + name + "!");

        System.out.println("Mis on Sinu perekonnanimi?");
        String lastName = scanner.nextLine();

        System.out.println("Väga meeldiv, " + name + " " + lastName + "!");

        System.out.println("Mis on Sinu lemmik värv?");
        String favoriteColor = scanner.nextLine();

        // + märgiga stringide liitmine on halb, sest iga liitmisega tekitatakse uus string ning see võtab mälu
        System.out.println("Nii tore, et Sinu lemmikvärv on " + favoriteColor +", " + name + " " + lastName + "!");

        // selle käsuga töötab tegelikult StringBuilder. 'ga saab suvalises kohas stringi rida vahetada
        System.out.printf("Nii tore, et Sinu lemmikvärv on %s, %s %s!\n",
                favoriteColor, name, lastName);

        StringBuilder builder = new StringBuilder();

        // builder'il saab nüüd kutsuda välja append meetodit, mis toimib dünaamiliselt ja mälu juurde ei vaja
        builder.append("Nii tore, et Sinu lemmikvärv on ");
        builder.append(favoriteColor);
        builder.append(", ");
        builder.append(name);
        builder.append(" ");
        builder.append(lastName);
        builder.append("!");

        // System.out.println(builder.toString());
        
        String fullText = builder.toString();

        System.out.println(fullText);
        
        // Nii System.out.printf kui ka String.format kasutavad siseselt StringBuilder'it
        
        String text = String.format("Nii tore, et Sinu lemmikvärv on %s, %s %s!",
                favoriteColor, name, lastName);
        System.out.println(text);


    }
}
