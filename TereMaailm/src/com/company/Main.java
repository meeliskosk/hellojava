package com.company;

// KLASS:
// public - tähendab, et klass, meetod või muutujad on avalikud progammis kasutamiseks
// class - java üksus või fail mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld - klassi nimi, mis on ka faili nimi
public class Main {

    // MEETOD:
    // meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse, eraldades komaga.
    // static - meetodi ees tähendab, et seda meetodit saab välja kutsuda ilma klassist objekti loomata
    // void - meetod ei tagasta midagi.
    // String[] - tähistab stringi massiivi
    // args - massiivi nimi, sisaldab käsurealt kaasa pandud parameetreid
    public static void main(String[] args) {

        // System.out.println - java meetod millega saab välja printid rida teksti
        // "Hello World!!" - meetodi parameeter
        System.out.println("Hello World!");
    }
}
