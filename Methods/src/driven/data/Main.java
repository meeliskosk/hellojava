package driven.data;

import java.util.Scanner;

public class Main {
    // Meetodid on koodiosad, mis grupeerivad mingit kinlat funktsionaalsust
    // Kui koodis on korduvaid osasid, võiks nende kohta meetodi teha
    public static void main(String[] args) {
        System.out.println("Hello (lihtne)!");

        // Selmet eraldi välja printida, saab ka meetodiga

        printHello();
        printHello(1);
        textPrinter("Kuidas läheb?");
        textPrinter(1, "Tere");
        textPrinter(1,"suur", true);
    }

    // Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello() {
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud kordi
    // nüüd on meetodil ka parameeter,
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes ; i++) {
            System.out.println("Hello!");
        }
    }

    // Teeme meetodi, mis prindib ette antud teksti välja
    static void textPrinter(String text){
        System.out.println(text);
    }

    // Tee meetod, mis prindib sisestatud teksti x korda
    static void textPrinter(int howManyTimes, String text){
        for (int i = 0; i < howManyTimes ; i++) {
            System.out.println(text);
        }
    }

    // Meetod, mis prindib sisestatud teksti x korda
    // lisaks saab öelda kas tahame kõik tähed teha suurteks või mitte
    static void textPrinter (int howManyTimes, String text, boolean caseUpper){
        for (int i = 0; i < howManyTimes ; i++) {
            if (caseUpper) {
                System.out.println(text.toUpperCase());
            } else {
                System.out.println(text.toLowerCase());
            }
        }
    }
    // Method overloading - on mitu sama nimega meetodit, aga erinevate parameetritega
}