package driven.data;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta e-mail");
        Scanner scanner = new Scanner(System.in);
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{2,50}\\.([a-z]{2,10})\\.?([a-z])?";
        String email = scanner.nextLine();

        if(email.matches("([a-z0-9\\._]{1,50})@[a-z0-9]{2,50}\\.([a-z]{2,10})\\.?([a-z])?")){
            System.out.println("Email korrektne");
        } else {
            System.out.println("Email ei ole korrektne");
        }
        Matcher matcher = Pattern.compile(regex).matcher(email);

        if(matcher.matches()){
            System.out.println("Email: " + matcher.group(0));
            System.out.println("Eespool @ märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
        }

        System.out.println("Sisesta isikukood");
        String regexI = "(3)[0-9]{6}";
        String isikukood = scanner.nextLine();

        Matcher matcherI = Pattern.compile(regexI).matcher(isikukood);

        System.out.println("Sünnikuupäeva osa: " + matcherI.group(0));

    }

    // Küsi kasutajalt isikukood
    // valideeri, kas on õige formaat
        // reaalne aasta
        //
    // ja prindi välja sünnipäev
    //

}
