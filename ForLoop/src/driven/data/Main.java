package driven.data;

public class Main {

    public static void main(String[] args) {

// 'ForLoop' tsüklil on kolm parameetrit:
//        1. tekitatakse muutuja(d) ning algväärtustatakse täisarv i'ga, mille väärtus hakkab tsükkli sees muutuma
//        2. tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks
//        3. tegevus mida iga tsükli lõpus korratakse, nt i++ == i = i + 1
// tsükkel jookseb seni kaua kuni parameetrid lubavad
// lühend 'fori'
// lõpmatu tsükkel:
// for( ; ; ) {
// System.out.println("Väljas on ilus ilm");
// }
        // tsükkel kordub 3x - i = 0, 1, 2
        for (int i = 0; i < 3; i++ ) {
            System.out.println("Väljas on ilus ilm");
        }

        // Prindi ekraanile numbrid 1 kuni 10

        for (int i = 1; i <= 10 ; i++) {
            System.out.println(i);
        }
        System.out.println();

        // Prindi ekraanile numbrid 24st kuni 167ni

        for (int i = 24; i <= 67 ; i++) {
            System.out.println(i);
        }
        System.out.println();

        // prindi ekraanile numbrid 18st 3ni

        for (int i = 18; i >=3; i--) {
            System.out.println(i);
        }
        System.out.println();
        // i = i + 2 => i += 2
        // i = i -4 => i -= 4
        // i = i * 3 => i *= 3 arvu iseenda korrutamine mingi numbriga
        // i = i / 2 => i /= 2
        // prindi ekraanile numbrid 2 kuni 10

        for (int i = 2; i <=10 ; i+=2) {
            System.out.println(i);
        }
        System.out.println();
        // Prindi ekraanile numbrid 10 kuni 20 ja 40 kuni 60

        for (int i = 10; i <=60 ; i++) {
            if (i<=20 || i >= 40){
                System.out.println(i);
            }
        }
        System.out.println();

        for (int i = 0; i <= 60; i++){
            if(i==21) {
                i = 40;
            }
            System.out.println(i);
        }
        System.out.println();
        // Prindi koik arvud mis jaguvad 3ga vahemikus 10 kuni 50

        for (int i = 10; i <= 50 ; i++) {
            if (i % 3 == 0){
                System.out.println(i);
            }
        }



    }
}
