package driven.data;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {
            // Try plokis otsitakse Exceptioneid (Erindeid)
            // Klass, mis kirjutab faili
            // Sellest klassist objekti loomisel antakse ette faili asukoht
            // faili asukoht võib olla ainult faili nimega kirjeldatud: output.txt
            // sel juhul kirjutatakse samasse kohta kus asub Main.class
                // C:\Users\opilane\Desktop\MK\ValiIT\WriteToFile\out\production\WriteToFile\driven\data\Main.class
            // või täispika asukohaga, nt C:\Users\opilane\Desktop\MK\varia
            FileWriter fileWriter = new FileWriter("test1.txt");
            fileWriter.write("Elas metsas mutionu" + System.lineSeparator());
            fileWriter.write(String.format("Elas metsas mutionu%n"));
            fileWriter.write("Elas metsas mutionu\r\n");
            fileWriter.close();
            // Catch plokis püütakse kinni kindlat tüüpi Exception
            // või kõik exceptionid mis pärinevad antud exceptionst
        } catch (IOException e) {
            // print.StackTrace tähendab, et prinditakse välja meetodite
            // hierarhia/ajalugu
            // seda on võimalik ära peita ja asendada oma veateatega
            //e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }
    }
}
