package driven.data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        System.out.println();
        try {
            FileReader fileReader = new FileReader(
                    "C:\\Users\\opilane\\Desktop\\MK\\ValiIT\\ReadFromFile\\readFile.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            // Kui readLine avastab, et järgmist rida tegelikult ei ole,
            // siis tagastab see meetod null
            while(line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
//        do {
//            line = bufferedReader.readLine();
//            if(line != null)
//                System.out.println(line);
//        } while (line != null);

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("Faili ei leitud");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
