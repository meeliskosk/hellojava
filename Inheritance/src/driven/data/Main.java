package driven.data;

public class Main {

    public static void main(String[] args) {

        Cat angora = new Cat();
        angora.setName("Miisu");
        angora.setAge(9);
        angora.setBreed("angora");
        angora.setWeight(4.34);
        angora.setHasFur(true);
        angora.printInfo();
        angora.catchMouse();
        angora.eat();

        System.out.println();

        Cat persian = new Cat();
        persian.setName("Kiisu");
        persian.setAge(3);
        persian.setBreed("persian");
        persian.setWeight(6.78);
        persian.printInfo();
        persian.eat();

        System.out.println();

        Dog tax = new Dog();
        tax.setName("Pontu");
        tax.setAge(5);
        tax.setBreed("tax");
        tax.setWeight(2.78);
        tax.printInfo();
        tax.eat();
        tax.playWithCat(angora);
        angora.catchMouse();

        // Lisa pärinevusahelast puuduvad klassid
        // Igale klassile lisa 1 muutuja ja 1 meetod mis on unikaalne sellele klassile

        Herbivore donkey = new Herbivore();
        donkey.setName("Donkey");
        donkey.setHabitat("Savannah");
        donkey.runWild();
        donkey.digest();
    }
}
