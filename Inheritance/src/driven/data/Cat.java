package driven.data;

public class Cat extends Animal {
    private boolean hasFur = true;
    public boolean isHasFur() {
        return hasFur;
    }
    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }
    public void catchMouse(){
        System.out.println("Püüdsin hiire kinni");
    }
}
