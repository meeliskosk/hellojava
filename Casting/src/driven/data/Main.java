package driven.data;

public class Main {

    public static void main(String[] args) {
	// Casting on teisendamine ühest arvutüübist teise
        byte a = 3;

        // Kui üks numbritüüp mahub teise sisse,
        // siis toimub automaatne teisendamine -- implicit casting
        short b = a;

        // kui numbritüüp ei mahu teise sisse,
        // siis peab kõigepealt ise veenduma, et see number
        // mahub teise numbri tüüpi
        // ja kui mahub, siis peab ise seda teisendama
        // seda nimetatakse -- explicit casting

        short c = 300;
        byte d = (byte)c;

        System.out.println(d);

        long e = 100000000000L;
        int f = (int) e;

        System.out.println(f);

        float h = 123.111111111111F;
        double g = (double)h;
        double i = h;

        System.out.println(g);

        double j = 55.111111111111;
        float k = (float)j;
        System.out.println(k);

        double l = 12E50; // s.o 12 * 10^50
        float m = (float)l;
        System.out.println(m);

        // arvu täpsemaks saab muuta
        int n = 3;
        double o = n;
        System.out.println(o);

        // aga vastupidi kustutab komakohad ära
        double p = 2.99;
        short q = (short)p;
        System.out.println(q);


        double r = 2.2;
        double s = 9;
        System.out.println((int)(r + s));

        float t = 12.555555F;
        double u = 23.555555;
        System.out.println(t / u);
    }
}
