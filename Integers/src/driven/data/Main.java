package driven.data;

public class Main {

    public static void main(String[] args) {

        // deklareerida ja talle väärtust anda, saab ka kahe sammuga
        // samm 1: deklareeri tüüp int (integer) number1 (muutuja nimi)
        int number1;

        // samm 2: deklaeeri muutuja 'number1' väärtus
        number1 = 3;

        // saab ka ühe sammuga
        int number2 = 3 - 10;

        System.out.println(number1);
        System.out.println(number2);

        // %n tekitab platvormi spetsiifilise reavahetuse (Windows: \r\n ja Linux/Mac \n)
        // Selle asemel saab kasutada ka System.lineSeparator()
        System.out.printf("%d ja %d summa on %d\r\n", number1, number2, number1 + number2);
        System.out.printf("%d ja %d summa on %d%n", number1, number2, number1 + number2);


        // ÜLESANNE: loo väljund number1 ja number2 korrutis on: vaja tekitada kas uus int muutuja, mis on korrutis, võib saab ka ühe tehtena
        int product = number1 * number2;
        System.out.printf("%d ja %d korrutis on %d\r\n", number1,number2, number1 * number2);

        int a = 3;
        int b = 14;

        // Sellise juhul on väljund String, sest liidame stringile numbri ja seejuures teisendadakse int stringiks.
        // luuakse tegelikult 3 stringi: 1) arvude summa 2) arvude summa 3 3) arvude summa 35.
        // kui soovid, et tehe enne ära tehakse, siis peaks a ja b sulgude vahele panema
        System.out.println("Arvude summa on " + (a + b));

        System.out.printf("Arvude %d ja %d jagatis on %d", number1, number2, number1 / number2);


        // kahe täisarvu jagamisel on tulemusel jagatise täisosa
        System.out.printf("Arvude %d ja %d jagatis on %d %n", b, a, b / a);

        int maxInt = 2147483647;
        int c = maxInt - 2;
        int d = maxInt + 2;

        System.out.println(c);
        System.out.println(d);

        int minInt = -2147483648;
        int e = minInt - 1;

        System.out.println(e);

        short f = 32199;
        // long väärtust andes peab numbrile L tähe lõppu lisama
        long g = 8000000000L;

    }
}
