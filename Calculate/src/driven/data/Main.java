package driven.data;

public class Main {

    public static void main(String[] args) {
	   int sum = summation (2,3);
       int sub = subtraction(4, 5);
       // System.out.printf("4 miinus 5 on %d%n", sub);
       // System.out.printf("Arvude 2 ja 3 summa on %d%n", sum);

       int[] numbers = new int[5];
       numbers = new int[] {1, 2, 3, 4, 5};

       // System.out.printf("Massiivi arvude summa on %d%n",sum);

       System.out.println(division(4,2));

       printNumbers(reverseArray(new int[]{1,2,3,4,5}));

       printNumbers(convertToIntArray(new String[]{"2","-12", "1", "17"}));
    }

	// teeme meetodi mis liidab kaks täisarvu kokku ja tagastab nende summa

    static int summation (int a, int b) {
        int sum = a + b;
        return sum;

    }

    // lahutamine

    static int subtraction (int c, int d){
        int sub = c - d;
        return sub;

    }

    // jagamine
    static double division (int e, int f) {
        int division = e / f;
        return (double)e / f;
    }

    // korrutamine
    static int multiplication (int g, int h) {
        int mult = g * h;
        return mult;
    }

    // Tee meetod, mille parameetriks on massiivi elemendid ning tagastab summa

    static int sum(int[]numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Meetod mis võtab massiivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1

    static int[] reverseArray(int[]numbers){
        int[] reversedNumbers = new int[numbers.length];
        for (int i = 0; i < numbers.length ; i++) {
            reversedNumbers[i] = numbers[numbers.length - i - 1];

        }
        return reversedNumbers;
    }
    static void printNumbers(int[] numbers){
        for (int i = 0; i < numbers.length ; i++) {
            System.out.println(numbers[i]);
    
   
        }
    }
    // Meetod, mis võtab parameetriks stringi massiiv
    // eeldusel, et tegelikult on seal sees numbrid
    static int[] convertToIntArray(String[] numbersAsText){
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        
        

        return numbers;
    }


}
