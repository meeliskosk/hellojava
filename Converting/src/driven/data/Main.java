package driven.data;

import java.util.Locale;
import java.util.Scanner;

public class Main {

      public static void main(String[] args) {
          // Küsi kasutajalt kaks numbrit
          // prindi välja nende summa
          Scanner scanner = new Scanner(System.in);

          System.out.println("Esimene nr:");

          // scanner.nextLine() tagastab alati stringi, seega kui soovime, et sistatud väärtus oleks int
          // on vaja kastutada Integer.parseInt meetodit mis muudaks stringi int'ks
          // kõigil numbritüüpidel on olemas klassid, millega saab erinevaid teisendusi teha

          int a = Integer.parseInt(scanner.nextLine());

          System.out.println("Teine nr:");

          int b = Integer.parseInt(scanner.nextLine());

          System.out.printf("%n%d + %d = %d%n", a, b, a + b);

          // Küsi kaks reaalarvu ja prindi nende jagatis
          System.out.println("Kolmas nr:");
          double c = Double.parseDouble(scanner.nextLine());

          System.out.println("Neljas nr:");
          double d = Double.parseDouble(scanner.nextLine());

          System.out.printf("%n%.2f / %.2f = %.2f%n", c, d, c / d);

          // Kui ma tahan näha 0,56 asemel 0.56
          // siis ütlen eraldi ette, et kasuta USA local'i

          System.out.println(Locale.getDefault());
          System.out.println(String.format(Locale.US,"%.2f", c / d ));




    }
}
