package driven.data;

public class WildAnimal extends Animal {
    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    private String habitat;
    public void runWild(){
        System.out.println("I'm running wild!");
    }

    @Override

    public String getName(){
        return "Metsloomal pole nime";
    }

}
