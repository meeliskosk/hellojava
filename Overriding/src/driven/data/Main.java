package driven.data;

public class Main {

    public static void main(String[] args) {
	    // Meetodi overriding ehk ülekirjutamine
        // päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle

        Dog pontu = new Dog();
        pontu.printInfo();

        System.out.println();

        Cat kass = new Cat();
        kass.printInfo();

        // kirjuta koera getAge üle nii, et kui koera vanus on null, siis näitaks 1
        // WildAnimal printinfo võiks kirjutada: metsloomal pole nime
        System.out.println();

        WildAnimal karu = new WildAnimal();
        karu.printInfo();



    }
}
