package driven.data;

public class Vehicle implements Driver {

    private int speed;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }


    @Override
    public int getMaxDistance() {
        return 0;
    }

    @Override
    public void drive() {

    }

    @Override
    public void stopDrivingAfterDistance(int AfterDistance) {

    }


}
