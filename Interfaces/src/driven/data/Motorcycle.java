package driven.data;

public class Motorcycle extends Vehicle implements DriverInTwoWheels {

    @Override
    public int getMaxDistance() {
        return 300;
    }

    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab");

    }

    @Override
    public void stopDrivingAfterDistance(int AfterDistance) {

    }

    @Override
    public void driveInRearWheel() {
        System.out.println("Mootorratas sõidab tagarattal");

    }
}
