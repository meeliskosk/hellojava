package driven.data;

// Pärineb klassist sõiduk ka kasutab Flyer liidest
public class Plane extends Vehicle implements Flyer, Driver {

    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    private void doCheckList(){
        System.out.println("Checklist täidetud");
    }
    private void startEngine(){
        System.out.println("Mootor käivitus");
    }

    @Override
    public int getMaxDistance() {
        return 0;
    }

    @Override
    public void drive() {

    }

    @Override
    public void stopDrivingAfterDistance(int AfterDistance) {

    }
}
