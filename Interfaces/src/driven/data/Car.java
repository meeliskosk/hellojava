package driven.data;

public class Car extends Vehicle implements Driver {

    private int maxDistance;

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    private String make;

    public Car(int maxDistance){
        this.maxDistance = maxDistance;
    }
    @Override
    public int getMaxDistance() {
        return maxDistance;
    }
    @Override
    public void drive() {
        System.out.println("I'm moving");
    }
    @Override
    public void stopDrivingAfterDistance(int afterDistance) {
        if (afterDistance > maxDistance) {
            System.out.printf("Car has reached maximum distance %s%n", maxDistance);
        }
    }

    @Override
    public String toString () {
        return "Auto andmed: " + make + " ,maksimum distants: " + maxDistance;
    }
    }