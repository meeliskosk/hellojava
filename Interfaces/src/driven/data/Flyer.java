package driven.data;

// Interface ehk liides sunnib seda kasutavat klassi omama
// liideses olevaid meetodeid
// sama tagastuse tüübi ja parameetrite komboga
// Iga klass, mis liidest kastuab, määrab ise meetodi sisu
public interface Flyer {

    void fly();
}
