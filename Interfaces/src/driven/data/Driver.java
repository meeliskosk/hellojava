package driven.data;

public interface Driver {

    int getMaxDistance();
    void drive();
    void stopDrivingAfterDistance(int AfterDistance);
}
