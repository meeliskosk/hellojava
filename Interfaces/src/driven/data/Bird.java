package driven.data;

// Bird klass kasutab Flyer liidest
public class Bird implements Flyer {
    @Override
    public void fly(){
        jumpUp();
        System.out.println("lind lendab");
    }

    private void jumpUp(){
        System.out.println("Lind hüppas õhku");
    }
}
