package driven.data;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Flyer bird = new Bird();
        Flyer plane = new Plane();
        System.out.println();
        bird.fly();
        System.out.println();
        plane.fly();

        List<Flyer> flyers = new ArrayList<Flyer>();

//        flyers.add(bird);
//        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);

//        for (Flyer flyer: flyers) {
//            flyer.fly();
//        }


        System.out.println();

        Driver bmw = new Car(600);

        bmw.drive();

        Car audi = new Car(500);
        audi.setMake("audi");
        System.out.println(audi);










    }

    // Lisa liides Drivers, klass Car.
    // Mõtle kas lennuk võiks mõlemad kasutada Drivers liidest
    // Drivers liideses võiks olla 3 meetodi kirjeldust (nimi, tagastusväärtus ja parameetrid)
    // int getMaxDistance()
    // void drive()
    // void stopDriving(int afterDistance) - saad määrata mis distantsi pärast lõpetab sõitmise
    // Pane auto ja lennuk mõlemad kasutama seda liidest
    // Lisa mootorratas
}
