package driven.data;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	 String sentence = "Väljas on ilus ilm ja päike paistab";

	 // Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
     // "|" tähendab regulaaravaldises "või"
	 String[] words = sentence.split(" ja | |,");

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        // Liitmist saab kutsuda esile järgnevalt,
        // esimene parameeter on eraldaja ja teine massiiv
        // eraldajaid võib olla igasuguseid, neid nimetatakse "Escape symbol"
        // kõik escape'id algavad "\"
        String newSentence = String.join("\b", words);
        System.out.println(newSentence);

        // Jutumärkidega printimine
        System.out.println("Juku \007");

        // Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale
        // eraldades tühikuga. Seejärel liidab need numbrid kokku ja prindib vastuse

        System.out.println("Sisesta arvud mida tahad omavahel liita, eraldades tühikuga");

        Scanner scanner = new Scanner(System.in);
        String numbersText = scanner.nextLine();

        String[] numbers = numbersText.split(" ");


        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        int sum = 0;
        for (int i = 0; i < numbers.length ; i++) {
            sum = sum + Integer.parseInt(numbers[i]);

        }

        String joinedNumbers = String.join(",",numbers);
        System.out.println(sum);
        System.out.printf("Arvude %s summa on %d%n",joinedNumbers, sum);


    }
}
