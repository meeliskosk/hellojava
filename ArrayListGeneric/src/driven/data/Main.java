package driven.data;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Generic arraylist on selline kollektsioon,
        // kus objekti loomise hetkel peame määrama, mis tüüpi
        // elemente see sisaladama hakkab

        List list = new ArrayList();
        // VS, kus kõik elemendid on arvud
        List<Integer> numbers = new ArrayList<Integer>();

        numbers.add(10);
        numbers.add(Integer.valueOf("10"));
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }
    }
}
