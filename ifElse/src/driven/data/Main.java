package driven.data;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Sisesta number
        System.out.printf("Sisesta number%n");
        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        // Kui arv on 4, siis prindi arv on 4
        // Kui arv on negatiivne, siis kontrolli kas arv on suurem kui -10 ja prindi tekst
        // muul juhul kontrolli kas arv on suurem kui 20 ja prindi tekst
        if(a == 4) {
            System.out.printf("%d on neli", a);
        } else {
            if (a < 0 && a >-10) {
                System.out.printf("%d on negatiivne ja suurem kui -10", a);
            } else {
                if (a > 20) {
                    System.out.printf("%d on suurem kui 20", a);
                }
            }
        }
    }
}
