package driven.data;

public class Main {

    static int a = 3;

    public static void main(String[] args) {
	    int b = 7;
        System.out.println(b + a);

        a = 0;
        System.out.println(b + a);

        System.out.println(increaseByA(10));

        // shadowing, ehk seespool defineeritud muutuja
        // varjutab väljaspool oleva muutuja
        int a = 6;

        System.out.println(b + a);
        System.out.println(increaseByA(10));

        // Nii saan klassi muutuja väärtust muuta
        Main.a = 8;

        // Meetodid defineeritud väärtuse muutmine
        a = 5;
        System.out.println(b + a);

    }

    static int increaseByA(int b){
        int a = 4;
        return b += a;
    }
}
