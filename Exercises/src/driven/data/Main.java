package driven.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // 1. Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala ekraanile

        System.out.println("Ülesanne 1: ");

        calculateCircleArea(1.32);

        System.out.println();
        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
        // ja mille sisendparameetriks on kaks stringi
        // meetod tagastab kas tõene või vale selle kohta,
        // kas stringid on võrdsed

        System.out.println("Ülesanne 2:");

        checkString("mina", "mina");

        System.out.println();
        // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv
        // ja mis tagastab stringide massiivi
        // Iga sisend-massiivi elemendi kohta olgu tagastatvas massiivis samapalju 'a' tähti

        System.out.println("Ülesanne 3: ");

        int[]numbers = {2,4,6,8,};
        String[]words = convertToStringArray(numbers);
        for (int i = 0; i < words.length  ; i++) {
            System.out.println(words[i]);

        }
        System.out.println();

        // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
        // sisestada saab ainult aastaid vahemikus 500-2019 (Veateade, kui ei ole selles vahemikus)
        // ja tagastab kõik sellel sajandil esinenud liigaastad
        System.out.println("Ülesanne 4: ");

        List<Integer> years = leapYearsInCentury(1920);
        for (int year: years) {
            System.out.println(year);

        }


        System.out.println();
        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning
        // list riikidega kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString()
        // nii, et see tagastab riikide nimekirja eraldades komaga.
        // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega
        // ning prindi välja selle objekti toString() meetodi sisu.
        System.out.println("Ülesanne 5: ");
        Language language = new Language();
        language.setLanguageName("English");

        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("China");
        countryNames.add("UK");
        countryNames.add("India");
        countryNames.add("Canada");

        language.setCountryNames(countryNames);

        System.out.println(language.toString());



    }

    public static void calculateCircleArea(double radius) {
        System.out.println("Ringi pindala on: " + Math.PI * radius * radius);
    }
    public static void checkString(String firstWord, String secondWord) {
        boolean sameWord;
        if (firstWord.equals(secondWord)) {
            sameWord = true;
        } else {
            sameWord = false;
        }
        System.out.println(sameWord);
    }


    static String printALetters(int count) {
        String word = "";
        for (int i = 0; i < count; i++) {
            word += "a";
        }

        return word;

    }
    static String[] convertToStringArray(int[] numbers ){
        String[] numbersAsString = new String[numbers.length];
        for (int i = 0; i < numbers.length ; i++) {
            numbersAsString[i] = printALetters(numbers[i]);
        }
        return numbersAsString;
    }

    static List<Integer> leapYearsInCentury(int year){
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;

        List<Integer> years = new ArrayList<Integer>();

        for (int i = 2020; centuryStart <= i; i -= 4) {
            if(i <= centuryEnd && i % 4 == 0){
                years.add(i);
            }
        }
        return years;
    }






}
