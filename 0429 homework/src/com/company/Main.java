package com.company;

public class Main {

    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }


    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause,
    // kus iga stringi vahel on tühik



   static String[] sentenceBuilder () {
        String[] words = new String[] {"Tere","tere","vana","kere"};
        String[] ret = new String[4];
        ret[0]= words [0];
        ret[1]= words [1];
        ret[2]= words [2];
        ret[3]= words [3];
        return ret;


   }



    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja
    // Tagastada lause.



    String[] thisIsAStringArray = { "Apple", "Banana", "Orange" };
    String delimiter = "-";
    StringBuilder sb = new StringBuilder();
        for String element : thisIsAStringArray ) {
        if (sb.length() > 0) {
            sb.append( delimiter );
        }
        sb.append( element );
    }
    String theString = sb.toString();
    System.out.println( theString );




    // Vaata eelmiset ülesannet, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldab

    // Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle

    static double average (int[] myNumbers){
        return sum(myNumbers) / myNumbers.length;
    }

    static double calcAverage(int[] numbers) {

        double sum = 0;
        for (int i=0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }

        double result = sum / numbers.length;
        System.out.println(result);

        return result;
    }

    int[] myNumbers = {1,2,3,4,5,6,7,8};
    double myNumbersAverage = calcAverage(myNumbers);




    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.
    static double percentageCalculate (double a, double b) {
        return a / (double)b * 100;
    }


    // Meetod, ringi ümbermõõdu raadiuse järgi

    static int circle (double radius) {

        double circumference= Math.PI * 2*radius;
        return (int) circumference;
    }

}
