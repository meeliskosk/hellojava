package driven.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[]numbers = new int[6];


        // lisa massiivi 5 numbrit;
        // eemalda iga teine number
        numbers[0] = 3;
        numbers[1] = 5;
        numbers[2] = -2;
        numbers[3] = -2;
        numbers[4] = 12;
        numbers[5] = 32;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // Tavalisse array listi võib lisada ükskõik mis tüüpi elemente
        // aga elemente küsides pean teadma mis tüüpi element kus asub
        // ning peab selleks tüübiks ka cast-ima.
        List list = new ArrayList();

        list.add("Tere");
        list.add(2);
        list.add(false);
        double money = 24.44;
        list.add(money);


        int a = (int)list.get(1);
        String word = (String) list.get(0);

        int sum = 3 + (int)list.get(1);

        String sentence = list.get(0) + " hommikust!";
        System.out.println(sentence);

        if(Integer.class.isInstance(a)){
            System.out.println("a on int");
        }

        list.add(2, "head aega!");

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        List otherList = new ArrayList();

        otherList.add(2);
        otherList.add("Mis toimub");
        otherList.add(true);
        otherList.add(3.145345);

        list.addAll(otherList);
        System.out.println();

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }
        System.out.println();

        // otsib kas listis on midagi sees
        if(list.contains(money)){
            System.out.println("money on listis");
        }
        System.out.printf("money asub listis indeksil %d%n", list.indexOf(money));
        System.out.printf("2 asub listis indeksil %d%n", list.indexOf(2));

        // saab kutstutada konkreetseid elemente või indeksi järgi.
        list.remove(3);
        list.remove(money);
        System.out.println();

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }
        list.
    }
}

